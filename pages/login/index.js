import React, {useEffect, useState, useContext} from 'react';
import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import {GoogleLogin} from 'react-google-login';
import styles from '../../styles/index.module.css'


export default function index() {

    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        e.preventDefault();

        fetch("https://serene-citadel-92648.herokuapp.com/api/users/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then(res => res.json())
            .then(data => {
                
                let token = data.accessToken
                localStorage.setItem('token', data.accessToken)

                if(data.accessToken){

                    fetch('https://serene-citadel-92648.herokuapp.com/api/users/details', {
                        headers: {
                            "Authorization": `Bearer ${token}`,

                        }
                    })
                    .then(res => res.json())
                    .then(data => {

                        localStorage.setItem('id', data._id)
                        setUser({
                            id: data._id
                        })

                        setEmail('');
                        setPassword('');

                        Swal.fire({
                            icon: "success",
                            title: "Successfully Logged in.",
                            text: "Thank you for logging in."
                        })

                        Router.push('/')
                    })
                    

                } else {

                    if(data.error === 'does-not-exist'){
                        Swal.fire({
                            icon: "error",
                            title: "Authentication Failed.",
                            text: "User does not exist."
                        })  
                    }else if (data.error === 'incorrect-password'){
                        Swal.fire({
                            icon: "error",
                            title: "Authentication Failed.",
                            text: "Password is Incorrect"
                        })  
                    }else if (data.error === 'login-type-error') {
                        Swal.fire({
                            icon: "error",
                            title: "Login Type Error.",
                            text: "You may have registered through a different method."
                        }) 
                    }

                    
                }
            });

    }

    useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticateGoogleToken(response){

        fetch('https://serene-citadel-92648.herokuapp.com/api/users/verify-google-id-token', {
            method: 'POST',
            headers:{
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        })
        .then(res => res.json())
        .then( data => {
            console.log(data)

            if (typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'google.auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.',
                        'error'
                        )
                }else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure.',
                        'error'
                        )
                }
            }
        })
    }

    function retrieveUserDetails(accessToken) {
        fetch('https://serene-citadel-92648.herokuapp.com/api/users/details', {
            headers: { Authorization: `Bearer ${accessToken}`}
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
            })
            localStorage.setItem('id', data._id)
            Router.push('/records')
            
        })
    } 

    return (

        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" className="btn-block">
                    Login
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" className="btn-block" disabled>
                    Login
                </Button>
            }

            <GoogleLogin 

                clientId="742120983695-pqer2dgghjeqoglh1htlg6fgpl02n9oj.apps.googleusercontent.com"
                buttonText="Google Login"
                onSuccess={authenticateGoogleToken}
                onFailure={authenticateGoogleToken}
                cookiePolicy={'single_host_origin'}
                className="w-100 text-center my-4 d-flex justify-content-center"
            />
        </Form>
    )
}