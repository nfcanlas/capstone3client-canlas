import React, { useContext } from "react";
import Link from "next/link";
import { Navbar, Nav } from "react-bootstrap";
import UserContext from "../UserContext";

export default function NavBar() {
	const { user } = useContext(UserContext);

	return (
		<Navbar bg="dark" expand="lg">
			{user.id !== null ? (
				
				<Link href="/records">
				<a  className="navbar-brand text-light">WalletPH</a>
				</Link>

				)
				:(
				<Link href="/login">
				<a  className="navbar-brand text-light">WalletPH</a>
				</Link>
				)	
			}
			
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{user.id !== null ? (
						<React.Fragment>
							<Link href="/category">
								<a className="nav-link text-light" role="button">
									Categories
								</a>
							</Link>
							<Link href="/records">
								<a className="nav-link text-light" role="button">
									Records
								</a>
							</Link>
							<Link href="/chart/MonthlyExpense">
								<a className="nav-link text-light" role="button">
									Monthly Expense
								</a>
							</Link>
							<Link href="/chart/MonthlyIncome">
								<a className="nav-link text-light" role="button">
									Monthly Income
								</a>
							</Link>
							<Link href="/logout">
								<a className="nav-link text-light" role="button">
									Logout
								</a>
							</Link>
						</React.Fragment>
					) : (
						<React.Fragment>
							<Link href="/login">
								<a className="nav-link text-light" role="button">
										Login
								</a>
							</Link>
							<Link href="/register">
								<a className="nav-link text-light" role="button">
									Register
								</a>
							</Link>
						</React.Fragment>
					)}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}